
var response; // AJAX JSON response
var search_images;

function AjaxObjJson(meth,url){

	var x = new XMLHttpRequest();
	x.open(meth,url,true);
	x.setRequestHeader("Content-type","application/json");
	return x;
}

function AjaxObj(meth,url){

	var x = new XMLHttpRequest();
	x.open(meth,url,true);
	x.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	return x;
}


function AjaxReturn(x){
	if(x.readyState ==4 && x.status == 200)
		return true;

	return false;
}

function sendSearchRequest(){
    
const input_elem = document.getElementById("search_text");
const cb_elem = document.getElementById("cb-images");
    
    if(input_elem !== null){
        let txt = input_elem.value.trim();
        //console.log(txt);
        let url_string = 'https://www.googleapis.com/customsearch/v1?key='+googleKey+'&cx='+googleId+'&q='+txt;
        
        if(cb_elem.checked){
            search_images = true;
            url_string += '&searchType=image';
        }else{
            search_images = false;
        }
        
        const uri = encodeURI(url_string);
        var ax = AjaxObjJson('GET',uri); 
        ax.onreadystatechange = function() {
            if(AjaxReturn(ax)){
                let res = ax.responseText;
                response = JSON.parse(res);
                
                console.log(response);
                
                if(search_images){
                    updateImageResults(response);
                }
                else{
                    updateNormalResults(response);
                    updateImageResultsX(response);
                }
                
            }
        }

        ax.send(null);
    }
    
}

function getImageHtml(item){
    let htm =  '<div class="img-thumbnail"><a href="'+item['link']+'"><img src ="'+item['link']+'" alt="'+item['title']+'"/></a></div>';
//    console.log(htm);
    return htm;
}

function getImageHtmlX(item){
    let htm =  '<div class="img-thumbnail"><a href="'+item.pagemap.cse_image[0].src+'"><img src ="'+item.pagemap.cse_image[0].src+'" alt=""/></a></div>';
//    console.log(htm);
    return htm;
}

function getResultsHtml(item){
    let htm =  '<p class="link-thumbnail"><a class="thumbnail" href="'+item['link']+'"><b>'+item['title']+'</b><br>'+item['snippet']+'</a></p>';
//    console.log(htm);
    return htm;
}

function updateImageResults(resp) {
    
    const div= document.getElementById('div-images');
    if(div === null) return;
        
    let html = '';
    if(typeof resp['items'] !== 'undefined'){
        const items = resp['items'].map(getImageHtml);
        html = items.join('');
    }
    
    div.innerHTML = html;
        
}

function updateImageResultsX(resp) {
    
    const div= document.getElementById('div-images');
    if(div === null) return;
        
    let html = '';
    if(typeof resp['items'] !== 'undefined'){
        const filtered = resp['items'].filter(item => { return typeof item['pagemap']['cse_image'] !== 'undefined' ;});
        const items = filtered.map(getImageHtmlX);
        html = items.join('');
    }
    
    div.innerHTML = html;
        
}

function updateNormalResults(resp){
    const div= document.getElementById('div-results');
    if(div === null) return;
        
    let html = '';
    if(typeof resp['items'] !== 'undefined'){
        let items = resp['items'].map(getResultsHtml);
        html = items.join('');
    }
    
    div.innerHTML = html;
    
};